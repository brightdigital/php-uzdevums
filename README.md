![Dashboard piemērs](https://bright.lv/php_uzdevuma_attels.png)

Izmantojot jebkuru no PHP framework, izveidot vienkāršu dashboard aplikācijas uzmetumu.

### Uzdevuma mērķi: ###

* Galvenā lapa - sagatavot vienkāršu izkārtojumu atbilstoši mockup attēlojumam, vizuālās lietas nav nepieciešams noslīpēt
* Autorizēšanās lapa - admin paneļa autorizēšanās
* Admin panelis - izveidot vienkāršu CRUD brīvi izvēlētam datu modelim, kura datus attēlo galvenajā lapā. Datus glabāt datubāzē


### Sagaidāmais rezultāts: ###
Uzdevuma mērķis ir pārbaudīt tehnoloģiju zināšanas, labo prakšu izmantošanu un koda strukturēšanu.

Uzdevuma rezultātu iesūtīt Git linka veidā, aptuveni uzskaitot, cik laika pavadīts pie paveiktā.
